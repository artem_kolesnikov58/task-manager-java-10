package ru.kolesnikov.tm.api;

import ru.kolesnikov.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
