package ru.kolesnikov.tm.service;

import ru.kolesnikov.tm.api.ICommandRepository;
import ru.kolesnikov.tm.api.ICommandService;
import ru.kolesnikov.tm.model.TerminalCommand;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

}
